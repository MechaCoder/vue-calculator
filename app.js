var app = new Vue({
    el: '#app',
    data: {
        total: 0,
        subtotal: 0
    }
});

function addtoSubTotal(intToBeadded){
    
    if (app.subtotal === 0){
        app.subtotal = intToBeadded;
        return true
    }

    app.subtotal = parseInt( "" + app.subtotal + intToBeadded );

}

function actionClick(action){

    if(action === '+'){
        app.total = app.total + app.subtotal;
        console.log(app.total);
    }

    if(action === '-'){
        app.total = app.total - app.subtotal;
    }

    app.subtotal = 0
}